﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace petle
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Zadanie 1
            //for (int x = 0; x <= 10; x++)
            //{
            //    int y = 3 * x;
            //    Console.WriteLine("x = {0}, y = {1}", x, y);
            //}

            //Zadanie 2
            //int x = 0;
            //do
            //{
            //    int y = 3 * x;
            //    Console.WriteLine("x = {0}, y = {1}", x, y);
            //    x++;
            //} while (x <= 10);

            //Zadanie 3
            int x = 0;
            while (x <= 10)
            {
                int y = 3 * x;
                Console.WriteLine("x = {0}, y = {1}", x, y);
                x++;
            }
        }
    }
}
